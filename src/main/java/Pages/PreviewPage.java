package Pages;

import Services.Constants;
import Services.CustomService;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

/**
 * Created by andriiz on 05.09.17.
 */
@Log4j
public class PreviewPage {
    private final WebDriver driver;
    private CustomService service;

    public PreviewPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }


    @FindBy(xpath= ".//button[contains(text(),'Add to Cart')][@data-toggle='modal']")
    private WebElement addToCart;

    @FindBy (xpath = ".//*[@class='template-shopping-options recommended']//ul//li//*[contains(@id, 'sc-add-offer')]")
    private List<WebElement> onCartOffers;

    @FindBy (xpath = ".//*[@class='template-shopping-options upsells']//ul//li//*[contains(@id, 'sc-add-offer-oncart')]")
    private List<WebElement> onTemplateOffers;

    @FindBy(id="cart-summary-checkout")
    private WebElement checkoutButton;

    @FindBy(xpath = "//*[contains(@class,'upsells')]/ul/li/div/a[contains(text(),'Added')]")
    private WebElement addedOncart;

    @FindBy(xpath = "//*[contains(@class,'recommended')]/ul/li/div/a[contains(text(),'Added')]")
    private WebElement addedOnTemplate;

    Random random = new Random();





    public void addTemplate() {
        service.waitElement("//*[contains(@id,'preview-add-to-cart')]");
        service.waitClicable(addToCart);
        addToCart.click();
        log.info("add template to cart");
    }

    public void waitCheckoutButton(){
        service.waitClicable(checkoutButton);
    }


    public void addOnTemplateOffer(){
        Integer randTemplateOffer = random.nextInt(onTemplateOffers.size());
        service.waitClicable(onTemplateOffers.get(randTemplateOffer));
        onTemplateOffers.get(randTemplateOffer).click();
    }

    public void addOnCartOffer(){
        Integer randOncart = random.nextInt(onCartOffers.size());
        service.waitElement(".//*[@class='js-service-name']");

        service.waitClicable(onCartOffers.get(randOncart));
        onCartOffers.get(randOncart).click();

    }

    public void clickCheckoutButton(){

        service.waitForCheckoutButtonToBeEnabled(checkoutButton);
        checkoutButton.click();
    }

    public void addTemplateWithTwoOffers(){
        addTemplate();
        addOnCartOffer();
        addOnTemplateOffer();
        clickCheckoutButton();
    }





}
