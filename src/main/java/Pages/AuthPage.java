package Pages;

import Services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Admin on 26.08.2017.
 */
public class AuthPage {
    private final WebDriver driver;
    private CustomService service;

    public AuthPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(xpath = ".//input[@type='email']")
    private WebElement emailField;

    @FindBy(xpath = ".//span[@id='id-index-continue-button']/button")
    private WebElement continueButton;

    @FindBy(xpath = ".//input[@type='password']")
    private WebElement passwordField;

    @FindBy(id="id-password-login-button")
    private WebElement loginButton;

    @FindBy(xpath = ".//div[@class='notification__container']")
    private WebElement errorNotification;

    @FindBy(id = "id-general-facebook-button")
    WebElement facebookLoginButton;

    public void setEmail(String text){
        emailField.sendKeys(text);
    }

    public void clikContinue(){
        continueButton.click();
    }

    public void waitForLoginClicable(){
        service.waitClicable(loginButton);
    }

    public void setPassword(String text){
        passwordField.sendKeys(text);
    }

    public void clickLogIn(){
        loginButton.click();
    }

    public boolean notificationIsEnable(){
        return errorNotification.isDisplayed();
    }

    public String errorText(){
        return errorNotification.getText();
    }

    public void clearEmailField(){
        errorNotification.clear();
    }

    public void clickFacebookButton(){
        facebookLoginButton.click();
    }

    public void login(String email,String password){
        setEmail(email);
        clikContinue();
        waitForLoginClicable();
        setPassword(password);
        clickLogIn();
    }
}
