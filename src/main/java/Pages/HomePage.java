package Pages;

import Services.Constants;
import Services.CustomService;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Admin on 26.08.2017.
 */
@Log4j
public class HomePage {

    private final WebDriver driver;
    private CustomService service;

    public HomePage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy (xpath = ".//input[@name='keywords']")
    private WebElement search;

    @FindBy (id = "searchFormSubmit")
    private WebElement searchButton;

    public void fillInSearchField(String text){
        search.sendKeys(text);
    }

    public void clickSearchButton(){
        service.waitClicable(searchButton);
        searchButton.click();
    }





}
