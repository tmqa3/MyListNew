package Pages;

import Services.Constants;
import Services.CustomService;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by andriiz on 05.09.17.
 */
@Log4j
public class HomePageElements {
    private final WebDriver driver;
    private CustomService service;

    public HomePageElements(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy (xpath = ".//input[@name='keywords']")
    private WebElement search;

    @FindBy (id = "searchFormSubmit")
    private WebElement searchButton;


    public void fillInSearchField(String text){
        search.sendKeys(text);
    }

    public void clickSearchButton(){
        service.waitClicable(searchButton);
        searchButton.click();
    }


    public void searchTemplate(String template){
        fillInSearchField(template);
        log.info("fill in search field template number"+ Constants.TEMPLATE);
        clickSearchButton();
        log.info("click search button");
    }
}
