package Pages;

import Services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Admin on 26.08.2017.
 */
public class AccountPage {
    private final WebDriver driver;
    private CustomService service;

    public AccountPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "app-account-menu")
    private WebElement accountMenu;



    public void waitClicableAccountMenu(){
        service.waitClicable(accountMenu);
    }

    public boolean isDisplayedAccountMenu(){
       return accountMenu.isDisplayed();
    }




}
