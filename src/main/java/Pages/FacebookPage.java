package Pages;

import Services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by andriiz on 28.08.17.
 */
public class FacebookPage {
    private final WebDriver driver;
    private CustomService service;

    public FacebookPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "pass")
    private WebElement passwordField;

    @FindBy(id = "loginbutton")
    WebElement loginButton;

    @FindBy(xpath = ".//button[contains(text(),'Continue as')]")
    WebElement continueButton;

    public void typeEmail(){
        emailField.sendKeys("andriiz-for_test@templatemonster.me");
    }

    public void typePassword(){
        passwordField.sendKeys("1q2w3e4r5t6y");
    }

    public void loginFacebook(){
        typeEmail();
        typePassword();
        loginButton.click();
    }

    public void waitForClicableContinueButton(){
        service.waitClicable(continueButton);
    }

    public void clickContinueButton(){
        continueButton.click();
    }
}
