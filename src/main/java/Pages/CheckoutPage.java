package Pages;

import Services.Constants;
import Services.CustomService;
import Services.PaymentSystem;
import Services.User;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by andriiz on 05.09.17.
 */
@Log4j
public class CheckoutPage {

    private final WebDriver driver;
    private CustomService service;
    User user = new User();

    public CheckoutPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy (id = "signin3-form-email")
    private WebElement emailField;

    @FindBy(id = "signin3-new-customer")
    private WebElement continueCheckout;

    @FindBy (id = "billinginfo3-form-fullname")
    private WebElement nameField;

    @FindBy(id = "billinginfo3-form-phone")
    private WebElement phoneNumber;

    @FindBy (id="billinginfo3_form_countryiso2_chosen")
    private WebElement country;

    @FindBy(id="billinginfo3-form-postalcode")
    private WebElement zipCode;

    @FindBy(id="billinginfo3-form-cityname")
    private WebElement cityName;

    @FindBy(id="billinginfo3-form-submit")
    private WebElement registerButton;

    public void clickContinueButton(){
        service.waitClicable(continueCheckout);
        continueCheckout.click();
    }

    public void enterEmail(String email){
        service.waitClicable(emailField);
        emailField.sendKeys(email);
    }

    public void enterUserName(String name){
        service.waitClicable(nameField);
        nameField.sendKeys(name);
        log.info("enter User name");
    }

    public void enterPhoneNumber(String number){
        service.waitClicable(phoneNumber);
        phoneNumber.sendKeys(number);
        log.info("enter phone number");
    }


    public void enterZipCode(String code){
        zipCode.sendKeys(code);
        log.info("enter zip code");
    }

    public void enterCity(String city){
        nameField.click();
        service.waitClicable(cityName);
        cityName.sendKeys(city);
        log.info("enter city");
    }

    public void clickRegisterOnCheckout(){
        registerButton.click();
    }

    public void fillInRegistrationInformation(User user){
        service.waitClicable(registerButton);
        enterUserName(user.getName());
        enterPhoneNumber(user.getPhone());
        enterZipCode(user.getZipCode());
        service.waitClicable(cityName);
        enterCity(user.getCity());
        clickRegisterOnCheckout();
    }
    public void payBy(String paymentSystem){
        service.waitElement(".//*[@id='checkout-payment-buy-"+ Constants.PAYMENT+"']");
        WebDriverWait wait1 = new WebDriverWait(driver, 50);
        wait1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("checkout-payment-buy-"+paymentSystem)));
        WebElement payment = driver.findElement(By.id("checkout-payment-buy-"+paymentSystem));
        service.waitClicable(payment);
        payment.click();

    }

    public void fillEmailOnCheckout(String email){
        enterEmail(email);
        clickContinueButton();
    }


}
