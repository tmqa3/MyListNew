package Pages;

import Services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Admin on 27.08.2017.
 */
public class Header {

    private final WebDriver driver;
    private CustomService service;

    public Header(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "menu-favorites")
    private WebElement heart;

    @FindBy (id = "header-signin-link")
     WebElement signInLink;

    @FindBy (id = "app-account-menu")
    private WebElement accountMenu;

    @FindBy(xpath = ".//button/span[@class='app-sign-out-btn__inner']")
    private WebElement signOutButton;




    public void waitForHeartEnable(){
        service.waitClicable(heart);
    }

    public void clickToSignIn(){
        signInLink.click();
    }

    public void waitForAccountMenuClicable(){
        service.waitClicable(accountMenu);
    }

    public void clickToAccountMenu(){accountMenu.click();}

    public void waitForOutButtonClicable(){
        service.waitClicable(signOutButton);
    }

    public void clickToOutButton(){signOutButton.click();}
}
