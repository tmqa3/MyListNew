package MyArray;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Admin on 24.07.2017.
 */
public class MyList<T> implements List {
    int capacity = 10;
    T[] mainArray;
    T[] someArray;
    int size;
    //constructor start
    public MyList(){
        mainArray = (T[]) new Object[capacity];
    }
    public MyList(int capacity){
        mainArray = (T[]) new Object[capacity];
    }
    //constructor end
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size==0;
    }

    public boolean contains(Object o) {
        T genObject = (T) o;
        if(size!=0){
            for(int i=0;i<size;i++){
                if(mainArray[i].equals(genObject)){
                    return true;
                }else{return false;}
            }
        }else{
            System.out.println("aray is empty");
            return false;
        }
        return true;
    }

    public Iterator iterator() {
            return new MyIterator();
    }

    public Object[] toArray() {
        T[] newArray = (T[]) new Object[capacity];
        System.arraycopy(mainArray,0,newArray,0,size);
        return newArray;
    }

    public boolean add(Object o) {
        T genObject = (T) o;
        if(size<capacity){
            mainArray[size()]=genObject;
            size+=1;
            return true;
        }else if(size>=capacity){
            T[] biggerArray = (T[]) new Object[capacity+10];
            for (int i=0;i<size;i++){
                biggerArray[i]=mainArray[i];}
            biggerArray[size]=genObject;
            mainArray=biggerArray;
            size+=1;
            capacity+=10;
            return true;
        }

        return false;
    }


    public void showArray(){
        for (int i = 0; i<size;i++){
            System.out.print("["+mainArray[i]+"]");
        }
        System.out.println();
    }
    public boolean remove(Object o) {
        T genObject = (T) o;
        if(size!=0) {
            for(int i=0;i<size;i++){
                if(mainArray[i].equals(genObject)){
                    for(int j=i;j<size;j++){
                        mainArray[j]=mainArray[j+1];
                    }
                    size-=1;
                }else{return false;}
            }
        }else{
            System.out.println("array is empty");
            return false;
        }
        return true;
    }

    public boolean addAll(Collection c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            add(iterator.next());
        }
        return true;
    }

    public boolean addAll(int index, Collection c) {
        return false;
    }

    public void clear() {
        T [] newArrayMain = (T[]) new Object[capacity];
        mainArray = newArrayMain;
        size=0;
        capacity=10;
    }

    public Object get(int index) {
        if(index<=size){return mainArray[index];
        }else{
            System.out.println("elment with index "+index+" was not found");
            return false;
        }
    }

    public Object set(int index, Object element) {
        T genElement = (T) element;
        if(index<=size){
            return mainArray[index]=genElement;
        }else{
            return "not fount this index";
        }
    }

    public void add(int index, Object element) {
        T genElement = (T) element;
        T[] biggerArray = (T[]) new Object[capacity+10];
        if(index<=size || index<=size+1){
            for(int i=0;i<size;i++){
                if(i<index){
                    biggerArray[i]=mainArray[i];
                }else if(i==index){
                    biggerArray[i]=genElement;
                    biggerArray[i+1]=mainArray[i];
                    }else{biggerArray[i+1]=mainArray[i];}
            }
            size++;
            mainArray=biggerArray;
        }

    }

    public Object remove(int index) {
        if(size!=0 && index<=size){
            for(int i=index;i<size-1;i++){
            mainArray[i]=mainArray[i+1];}
            size--;
            return mainArray;
        }
        return null;
    }

    public int indexOf(Object o) {
        if(size!=0){
            for(int i=0;i<size;i++){
                if(mainArray[i].equals(o)){
                    return i;
                }
            }
        }
        return 0;
    }

    public int lastIndexOf(Object o) {
        return 0;
    }

    public ListIterator listIterator() {
        return null;
    }

    public ListIterator listIterator(int index) {
        return null;
    }

    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    public boolean retainAll(Collection c) {
        return false;
    }

    public boolean removeAll(Collection c) {
        return false;
    }

    public boolean containsAll(Collection c) {
        return false;
    }

    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    private class MyIterator implements Iterator{
        int cursor;
        public boolean hasNext() {
            return cursor!=size;
        }

        public Object next() {
            return hasNext() ? get(cursor++) : null;
        }

        public void remove() {

        }
    }

}