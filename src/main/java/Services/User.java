package Services;

import lombok.Builder;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

/**
 * Created by Admin on 05.09.2017.
 */
@Getter
public class User {
    WebDriver driver;
    CustomService service = new CustomService(driver);
    private String email = "andriiz-"+service.getRandomNumAsString()+"@templatemonster.me";
    private String name = "Test Name";
    private String country = "Ukraine";
    private String phone = "0987654321";
    private String zipCode = "123123";
    private String city = "Lviv";

}
