package Services;

import org.omg.PortableInterceptor.ServerRequestInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

/**
 * Created by Admin on 26.08.2017.
 */
public class CustomService {
    private WebDriver driver;

    public CustomService(WebDriver driver){
        this.driver=driver;
    }

    public void waitClicable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.withMessage("Element is not clickable");
        wait.until(ExpectedConditions.elementToBeClickable(element));

    }

    public void waitForCheckoutButtonToBeEnabled(WebElement checkoutButtonElement) {
        WebDriverWait wait = new WebDriverWait(driver, 50, 2000);
        wait.withMessage("Failed to wait that Checkout button to be enabled");

        wait.until((ExpectedCondition<Boolean>) d -> {
            String aClass = checkoutButtonElement.getAttribute("class");
            return !aClass.contains("disabled");
        });
    }

    public void waitUrl(String url){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void switchToLastWindow(){
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }



    public void goTo(String url){
        driver.get(url);
    }

    public void waitElement(String xpathElement){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathElement)));
    }

    public String getRandomNumAsString(){
        Random random = new Random();
        Integer rand = random.nextInt(99999);
        return rand.toString();
    }







}
