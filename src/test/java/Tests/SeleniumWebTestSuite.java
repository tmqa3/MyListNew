package Tests;

import Pages.Header;
import Pages.HomePage;
import Services.CustomService;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by andriiz on 14.08.17.
 */

public class SeleniumWebTestSuite extends BaseTest{
    @Test
    void test(){
        CustomService service = new CustomService(driver);
        service.goTo("https://www.templatemonster.com/");

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        Header header = PageFactory.initElements(driver, Header.class);
        driver.manage().window().maximize();
        header.waitForHeartEnable();
        header.clickToSignIn();
        service.switchToLastWindow();
        service.waitUrl("account");
        Assert.assertEquals(driver.getCurrentUrl(), "https://account.templatemonster.com/auth/#/", "Incorrect current url");
        // emailField.getAttribute("");
        //emailField.


//        WebElement fashionBtn = driver.findElement(By.id("categories-fashions"));
//        List<WebElement> subMenu = driver.findElements(By.xpath(".//*[@class='sub-menu-1 js-sub-menu-1 sub-menu-1_dropdown']/child::*/li"));
//        // WebElement favoriteFrom3Template = driver.findElement(By.xpath(".//*[@id='products']/li[3]/descendant::b[@class='tm-icon icon-heart-empty js-favorite-btn favorite-btn js-favorite']"));
//        WebElement account = driver.findElement(By.id("header-signin-link"));
//        WebElement emailField = driver.findElement(By.xpath(".//input[@type='email']"));

    }

}
