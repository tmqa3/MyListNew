package Tests;

import Pages.*;
import Services.Constants;
import Services.CustomService;
import Services.PaymentSystem;
import Services.User;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.Builder;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import sun.management.snmp.util.MibLogger;

/**
 * Created by andriiz on 04.09.17.
 */
@Log4j
public class PurchaseWithOffers extends BaseTest {


    @Test
    public void test(){
        User user = new User();

        CustomService service = new CustomService(driver);
        service.goTo(Constants.URL);
        driver.manage().window().maximize();

        HomePageElements homePage = PageFactory.initElements(driver, HomePageElements.class);
        homePage.searchTemplate(Constants.TEMPLATE);
        service.waitUrl(Constants.TEMPLATE+".html");

        PreviewPage previewPage = PageFactory.initElements(driver, PreviewPage.class);
        previewPage.addTemplateWithTwoOffers();
        service.waitUrl(Constants.URL+"checkout.php");

        CheckoutPage checkoutPage = PageFactory.initElements(driver, CheckoutPage.class);
        checkoutPage.fillEmailOnCheckout(user.getEmail());
        checkoutPage.fillInRegistrationInformation(user);
        checkoutPage.payBy(Constants.PAYMENT);

        service.waitUrl(Constants.PAYMENT);
        //Assert.assertEquals(driver.getCurrentUrl(),Constants.PAYMENT,"incorrect payment url ");
        //Assert.assertEquals();


    }
}
