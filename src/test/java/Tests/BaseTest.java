package Tests;

import Pages.*;
import Services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Random;

/**
 * Created by andriiz on 16.08.17.
 */
public class BaseTest {

    public WebDriver driver;



    @BeforeMethod
    void init () throws MalformedURLException{
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);




    }

    @AfterMethod
    void clear(){
        driver.quit();
    }
}
