package Tests;

import Pages.*;
import Services.CustomService;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Set;

/**
 * Created by Admin on 26.08.2017.
 */
public class LogInWithFacebook extends BaseTest{
    @Test
    public void test() {
        CustomService service = new CustomService(driver);
        service.goTo("https://www.templatemonster.com/");

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        Header header = PageFactory.initElements(driver, Header.class);
        AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
        AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);
        FacebookPage facebookPage = PageFactory.initElements(driver, FacebookPage.class);

        driver.manage().window().maximize();
        header.waitForHeartEnable();
        header.clickToSignIn();
        service.switchToLastWindow();
        service.waitUrl("account");
        authPage.clickFacebookButton();
        service.switchToLastWindow();
        service.waitUrl("facebook");
        facebookPage.loginFacebook();
        facebookPage.waitForClicableContinueButton();
        facebookPage.clickContinueButton();
        service.switchToLastWindow();
        header.waitForAccountMenuClicable();
        Assert.assertTrue(accountPage.isDisplayedAccountMenu(), "account menu is not display");
    }
}
