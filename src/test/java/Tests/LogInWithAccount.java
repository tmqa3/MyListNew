package Tests;

import Pages.*;
import Services.CustomService;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Admin on 26.08.2017.
 */
public class LogInWithAccount extends BaseTest{
    @Test
     void test(){
        CustomService service = new CustomService(driver);
        service.goTo("https://www.templatemonsterdev.com/");
       HomePageElements homePage = PageFactory.initElements(driver, HomePageElements.class);
       Header header = PageFactory.initElements(driver, Header.class);
       AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
       AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);
       FacebookPage facebookPage = PageFactory.initElements(driver, FacebookPage.class);

        driver.manage().window().maximize();
        header.waitForHeartEnable();
        header.clickToSignIn();
        service.switchToLastWindow();
        service.waitUrl("account");

//        authPage.setEmail("andriiz");
//        authPage.clikContinue();
//        service.waitElement(".//div[@class='notification__container']");
//        Assert.assertEquals(authPage.notificationIsEnable(), true,"error message is not displayed");
//        Assert.assertEquals(authPage.errorText(),"Please specify a valid email", "incorrect error message");
//        authPage.clearEmailField();

        authPage.login("andriiz@templatemonster.me", "123123");
        accountPage.waitClicableAccountMenu();
        Assert.assertTrue(accountPage.isDisplayedAccountMenu(), "account menu is not display");

        header.waitForAccountMenuClicable();
        header.clickToAccountMenu();
        header.waitForOutButtonClicable();
        header.clickToOutButton();

        service.waitUrl("https://account.templatemonster.com/auth");
        Assert.assertEquals(driver.getCurrentUrl(), "https://account.templatemonster.com/auth/?lang=en#/", "incorrect Url after logout");

    }
}
