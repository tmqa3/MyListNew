package Tests;



import lombok.extern.java.Log;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import lombok.Data;

@Log
public class  TestList{


    @Test
    public void testAddArray(){
        MyArray.MyList myList = new MyArray.MyList();
        myList.add(5);
        Assert.assertEquals(myList.size(),1,"add not work");
        //Reporter.log("test add all good");
        log.info("for list was add object");
    }



    @Test
    public void testClear(){
        MyArray.MyList myList = new MyArray.MyList();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.clear();
        Assert.assertEquals(myList.size(),0,"after clear size>0");
        log.info("List was a cleared");
    }
    @Test
    public void testIsEmpty(){
        MyArray.MyList myList = new MyArray.MyList();
        Assert.assertEquals(myList.isEmpty(),true,"isEmpty should show true");
        log.info("empty list show true in IsEmpty method");
        myList.add(7);
        Assert.assertEquals(myList.isEmpty(),false,"isEmpty should show false");
        log.info("not empty list show false in IsEmpty method");
    }
    @Test(description = "")
    public void testRemove(){
        MyArray.MyList myList = new MyArray.MyList();
        myList.add(5);
        myList.add(4);
        myList.remove(1);
        Assert.assertEquals(myList.size(),1,"remove not work");
        log.info("remove - element with index was removed");
    }
    @Test
    public void testIndexOf(){
        MyArray.MyList myList = new MyArray.MyList();
        for (int i=0;i<10;i++){
            myList.add(i);
        }
        Random rand = new Random();
        int randElement = rand.nextInt(10);

        Assert.assertEquals(randElement,myList.indexOf(randElement),"index incorrect");
        log.info("indexOf return correct index");
    }
    @Test
    public void removeIndex(){
        MyArray.MyList myList = new MyArray.MyList();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.remove(1);
        Assert.assertEquals(myList.size(),2,"Size incorrect after delete");
        Assert.assertEquals(myList.get(1),3,"value incorrect on this position");
    }


    @DataProvider
    public Object [][] getData(){
        return new Object[][]{{4,99},{5,98},{7,97}};
    }

    @Test(dataProvider = "getData", groups = {"Test"})
    public void addIndex(int index, int value){
        MyArray.MyList myList = new MyArray.MyList();
        for (int i=0;i<10;i++){
            myList.add(i);
        }
        myList.add(index,value);
        Assert.assertEquals(myList.size(),11,"after add(used index) - size incorrect");
        Assert.assertEquals(myList.get(index),value,"after add(used index) - value on this position incorrect, add to index: "+index);

    }

    @Test(groups = {"Test"})
    public void testIterator(){
        MyArray.MyList myList = new MyArray.MyList();
        for (int i=0;i<10;i++){
            myList.add(i);
        }
        Iterator testIterator = myList.iterator();
        Assert.assertEquals(testIterator.hasNext(),true,"Iterator function hasnext not work");
        for(int j=0;j<10;j++){
        Assert.assertEquals(testIterator.next(),j,"element with index "+j+" is incorrect");
        }

    }







}