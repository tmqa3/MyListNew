package Tests;

import Pages.AccountPage;
import Pages.AuthPage;
import Pages.Header;
import Pages.HomePageElements;
import Services.CustomService;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Admin on 26.08.2017.
 */
public class LogOut extends BaseTest{
    @Test
    void test() {
        CustomService service = new CustomService(driver);
        LogInWithAccount login = new LogInWithAccount();
        login.test();
        HomePageElements homePage = PageFactory.initElements(driver, HomePageElements.class);
        Header header = PageFactory.initElements(driver, Header.class);
        AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
        AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);


        header.waitForAccountMenuClicable();
        header.clickToAccountMenu();
        header.waitForOutButtonClicable();
        header.clickToOutButton();

        service.waitUrl("https://account.templatemonster.com/auth");
        Assert.assertEquals(driver.getCurrentUrl(), "https://account.templatemonster.com/auth/?lang=en#/", "incorrect Url after logout");
    }
}
